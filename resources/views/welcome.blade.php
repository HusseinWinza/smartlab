<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Smart Lab  - ICB</title>
  <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

      <link rel="shortcut icon" type="image/png" href="https://www.smartlab.co.tz/images/favicong.png"/>
      <link rel="stylesheet" rel="stylesheet" href="https://www.smartlab.co.tz/animations/aos.css" />
      <link rel="stylesheet" type="text/css" href="https://www.smartlab.co.tz/slick/slick-theme.css"/>    
      <link rel="stylesheet" type="text/css" href="https://www.smartlab.co.tz/slick/slick.css"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="https://www.smartlab.co.tz/css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="https://www.smartlab.co.tz/css/main.css">
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
      
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

      <link rel="stylesheet" href="./style.css">
      <link href="/css/style.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ url('/css/main.css') }}" />
</head>
<body>

 <!--::header part start::-->
 <div class="header"> 
  <div class="top-header">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-8"><div class="logo"><a href="https://www.smartlab.co.tz"><img src="https://www.smartlab.co.tz/images/smartlab-wg-logo.png" alt=" smartlab"></a></div></div>
          <div class="col-xs-4"><div class="menu-btn pull-right">≡</div></div>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="menu-list">
              <ul>
                <li><a href="https://www.smartlab.co.tz/#home">Home</a></li>
                <li><a href="https://www.smartlab.co.tz/startup">Startup</a></li>
                <li><a href="https://www.smartlab.co.tz/corporate">Corporate</a></li>
                <!--<li><a href="https://www.smartlab.co.tz/programs">Programs</a></li>-->
                <li><a href="https://www.smartlab.co.tz/news">News</a></li>
                <li><a href="https://www.smartlab.co.tz/#contacts">Contacts</a></li>
                <li><a href="https://www.smartlab.co.tz/talent" class="btn-top">Hire&nbsp;Talents</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- header part end-->




<!-- image header part start-->

<div class="sections section parallax-window" data-parallax="scroll" data-image-src="images/smart.png">
  <div class="skin"  style="background-color: rgba(0,0,0,0.7); background-attachment:fixed;">
      <div class="container">
          <div class="welcome">
              <div class="row">
                  <div class="col-sm-12 lr">					
                      <h1 data-aos="fade-up" data-aos-duration="1300" style="font-family: Gilroy;
                      font-style: normal;
                      font-weight: 800;
                      font-size: 56px;
                      line-height: 56px;
                      letter-spacing: 0.03em;
                      
                      color: #FFFFFF;">University Outreach Program</h1>
                      <h4 data-aos="fade-up" data-aos-duration="1400" style="font-family: Gilroy;
                      font-style: normal;
                      font-weight: 500;
                      font-size: 20px;
                      line-height: 24px;
                      
                      color: #FFFFFF;">As one of our differentiated and unique niches is to create a linkage between learning institutions and corporates and we seek to implement that through our University Outreach Program. </h4>	
                  </div>
                 
                  <button class="btn_1"> <a href="form.html">Register Now</a></button>
                  
              </div>	
          </div>	
      </div>
      <a href="#about" class="arrow-down" style="background:#333; height:300px;">
          <div class="double-arrows">
              <div class="animated bounce"><i class="fa fa-angle-double-down"></i></div>
          </div>
      </a>
  </div>
</div> 

<!-- image header part end-->


<!-- icb brief section start -->

<section class="section-2 space ble" id="about" style="background-color: #fff;">
  <div class="container">
      <div class="row v-space">
          <div class="col-sm-12 lr">
              <div class="titles center">About The Program</div>
              <div id="icb-brief" >
                <h3 style="color: black; text-align: center; font-family: Gilroy;
                font-size: 20px;
                line-height: 24px;
                text-align: justify;
                
                color: #000000;">  The university outreach program is an initiative to get universities produce high quality graduates who have all the necessary employability & innovation skills but also get university students to be employed sooner and to better jobs. </h3>
                <h3  style="color: black; text-align: center; font-family: Gilroy;
                font-size: 20px;
                line-height: 24px;
                text-align: justify;
                
                color: #000000;"> Due to Covid-19 Effects, this year we will conduct online training to university students in Tanzania as a capacity building program in the areas most critical to employment and also getting them to understand the innovation practices in relation to problem solving and coming up with ground breaking solutions. Participants will be able to participate in the online training through an online platform conducted by the SmartLab team. A corporate hackathon session and a Career Fair will follow immediately after the training as they get a platform to increase their interactions with employers/corporates who will be taking part.  </h3>
                  <p style="color: black;"></p>
              </div>
          </div>
      </div>
    


      <div class="row features" style="font-family: Gilroy;
      font-size: 15px;
      line-height: 125.8%;

    
      text-align: justify;
      
      color: #000000;
      ">
        <div class="col-md-4 col-sm-6 flex-box lr" data-aos="fade-up" data-aos-duration="1300">
       
            <div class="the-box"  style="  background-color:#F8F8F8;  border-top:4px solid #0DB04B;">
            <div class="myhead" style="color: #0DB04B;">Project Management</div>
                <div class="myconts" >As much as university is about earning a degree, it’s becoming increasingly apparent that graduates should also possess a bank of soft skills of which are critical to the modern workplace. </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-6 flex-box lr" data-aos="fade-up" data-aos-duration="1500" style="background-color: #F8F8F8;">
    
            <div class="the-box"  style="  background-color:#F8F8F8; border-top:4px solid #0DB04B;">       
            <div class="myhead " style="color: #0DB04B;;">Project Management</div>
                <div class="myconts">Most graduates come out of school with less or no knowledge of project planning and management hence fail to get better jobs presented to them and end up getting jobs at entry level such as assistants. Inorder to change that we equip them with project management skills.</div>
                <p>&nbsp;</p>
             </div>
        </div>

        <div class="col-md-4 col-sm-6 flex-box lr" data-aos="fade-up" data-aos-duration="1700">
       
            <div class="the-box"  style="  background-color:#F8F8F8;  border-top:4px solid #0DB04B;">
           
            <div class="myhead" style="color: #0DB04B;;">Human Centered Design </div>
                <div class="myconts">This training aims to introduce participants to creative problem solving techniques through human centered design approach and to instill the relevant skills and competencies as a means to enable participants to become effective problem solvers and hence become more appealing to employers </div>
            </div>
        </div>
   </div>
  </div>
</section>

<div class="client-list" style="background:#F8F8F8;">

<div id="DateCountdown" data-date="2014-01-01 00:00:00" style="width: 500px; height: 125px; padding: 0px; box-sizing: border-box; background-color: #E0E8EF"></div>
</div>

<!-- partner section  startend-->
<div class="client-list" style="background:#F8F8F8;;">
<div class="titles-w center" data-aos="fade-up">Event Calender</div>
     
   <div class="event-box">
     <h1>sdsdsdsdsd</h1>
</div>

</div>

<!-- Resource  section  startend-->

       


<!-- resource section  startend-->


<div class="resource main-section">

<div class="container">
  <div class="titles-w center" data-aos="fade-up">Resources</div>
<div class="row">
  <div class="col-md-2 col-md-offset-1">
    <div class="tab">

      <ul class="tab-list" >
        <li  class="sliding-u-l-r-l">
          <a  href="#activities" id="btnactivity">
            Activities
          </a>
        </li>

        <li  class="sliding-u-l-r-l">
          <a href="#articles" id="btnarticle">
            Articles
          </a>
        </li>

        <li  class="sliding-u-l-r-l">
          <a href="#publications" id="btnpublication">
            Publication
          </a>
        </li>
      </ul>
      <!-- <button class="tablinks" id="btnactivity">Activities</button>
      <button class="tablinks" id="btnpublication">Publication</button>
      <button class="tablinks" id="btnarticle">Article</button> -->
    </div>
  </div>

<
  <div class="col-md-8 col-md-offset-1">

    <div id="activities" class="mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="r-title">
            <h3>
              Latest Activities
            </h3>
            <p>
Check out our latest activities from previously University outreach session              </p>
          </div>
        </div>

        <div class="col-md-12">
          <div>
            <div id="carousel-example-generic" class="carousel slide mt-3" data-ride="carousel">
            
            
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active ">

                  <div class="row">
                 

                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 1.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                    
                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 1.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                  
                  </div>
                 
                
                </div>

                <div class="item  ">

                  <div class="row">
                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 2.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 3.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                  
                  </div>
                 
                
                </div>

                <div class="item  ">

                  <div class="row">
                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 1.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                    <div class="col-md-6 ml-2">
                      <div class="image-holder">
                        <img src="images/img 3.jpg" class="img-fluid" alt="">
                      </div>
                      <div class="text-holder">
                        <p class="text-white">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel qui adipisicing elit. Vel dos tit
                        </p>
                      </div>
                      
                    </div>

                  
                  </div>
                 
                
                </div>


              
                
              </div>
            
             
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="articles" class="mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="r-title">
            <h3>
              Latest Articles
            </h3>
          </div>
        </div>

        <div class="col-md-12">
          <div>
            <div id="carousel-example-generic" class="carousel slide mt-3" data-ride="carousel">
            
            
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active ">

                  <div class="row">
                    <div class="col-md-6 ml-2">
                      <div class="news" style="width:100%; box-shadow: none !important;">
                        <a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="" tabindex="0"><div style="background-position:center; background-size:cover; background-image:url(https://www.smartlab.co.tz/uploads/web-5e3abb4b8262a.jfif); min-height:200px;"></div></a>
                         <div class="date">
                             Aug, 2020                    </div>
                         <div class="heading" style="min-height:70px;">The launch of Smart Lab’s University outreach program, 2020 edition in partner...</div>
                         <div class="" style="padding:4px 10px;"><a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="news-btn" tabindex="0">Read More...</a></div>
                       </div>
                     
                      
                    </div>

                    <div class="col-md-6 ml-2">
                    
                      <div class="news" style="width:100%; box-shadow: none !important;">
                        <a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="" tabindex="0"><div style="background-position:center; background-size:cover; background-image:url(https://www.smartlab.co.tz/uploads/web-5e3abb4b8262a.jfif); min-height:200px;"></div></a>
                         <div class="date">
                             Aug, 2020                    </div>
                         <div class="heading" style="min-height:70px;">The launch of Smart Lab’s University outreach program, 2020 edition in partner...</div>
                         <div class="" style="padding:4px 10px;"><a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="news-btn" tabindex="0">Read More...</a></div>
                       </div>
                      
                    </div>

                  
                  </div>
                 
                
                </div>

                <div class="item  ">

                  <div class="row">
                    <div class="col-md-6 ml-2">
                      <div class="news" style="width:100%; box-shadow: none !important;">
                        <a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="" tabindex="0"><div style="background-position:center; background-size:cover; background-image:url(https://www.smartlab.co.tz/uploads/web-5e3abb4b8262a.jfif); min-height:200px;"></div></a>
                         <div class="date">
                             Aug, 2020                    </div>
                         <div class="heading" style="min-height:70px;">The launch of Smart Lab’s University outreach program, 2020 edition in partner...</div>
                         <div class="" style="padding:4px 10px;"><a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="news-btn" tabindex="0">Read More...</a></div>
                       </div>
                     
                      
                    </div>

                    <div class="col-md-6 ml-2">
                    
                      <div class="news" style="width:100%; box-shadow: none !important;">
                        <a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="" tabindex="0"><div style="background-position:center; background-size:cover; background-image:url(https://www.smartlab.co.tz/uploads/web-5e3abb4b8262a.jfif); min-height:200px;"></div></a>
                         <div class="date">
                             Aug, 2020                    </div>
                         <div class="heading" style="min-height:70px;">The launch of Smart Lab’s University outreach program, 2020 edition in partner...</div>
                         <div class="" style="padding:4px 10px;"><a href="https://www.smartlab.co.tz/read/the-launch-of-smart-labs-university-outreach-program-2020-edition-in-partnership-with-the-human-development-innovation-fund" class="news-btn" tabindex="0">Read More...</a></div>
                       </div>
                      
                    </div>

                  
                  </div>
                 
                
                </div>

                <div class="item  ">                
                </div>                  
              </div>               
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="publications" class="mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="r-title">
            <h3>
              Latest publications
            </h3>
            <p>
               <li>Publication 0ne</li>
               <li>Publication Two</li>
            </p>
          </div>
        </div>

        <div class="col-md-12">
          <div class="text-white">
            <li>Publication 0ne</li>
            <li>Publication Two</li>  </div>
        </div>
      </div>
    </div>

    
   
  
    <!-- <div id="publication">
      <section class="regular slider">
        <div>
          <img src="images/img-1.jpg">
         
        </div>
        <div>
          <img src="images/img-2.jpg">
        </div>
        <div>
          <img src="images/img-3.jpg">
        </div>
        <div>
          <img src="images/img-4.jpg">
        </div>
        <div>
          <img src="images/img-6.jpg">
        </div>
        <div>
       
        </div>
      </section>
    </div>
   -->
  
    <!-- <div id="article">
         
            <section class="regular slider">
              <div>
                <img src="images/img-1.jpg">
               
              </div>
              <div>
                <img src="images/img-2.jpg">
              </div>
              <div>
                <img src="images/img-3.jpg">
              </div>
              <div>
                <img src="images/img-4.jpg">
              </div>
              <div>
                <img src="images/img-6.jpg">
              </div>
              <div>
             
              </div>
            </section>
    </div> -->

  </div>
  </div>
</div>
</div>




<!-- <div > -->




<!-- Resourcer section  end-->

<!-- partner section  startend-->
<div class="client-list" style="background:#F8F8F8;;">
<div class="titles-w center" data-aos="fade-up">Partners</div>
    <div class="container">

        <div class="row responsivez v-space">
                              <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e4533aef3027.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e45394cae645.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e46c52ab2730."></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e46c5b8f2079.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e46c6726d6ea.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e46f20e829db.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e57c595d595c.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e57c69a30382.png"></div>
                        <div class="col-xs-3"><img src="https://www.smartlab.co.tz/uploads/web-5e57cb5c6c969.png"></div>
                      </div>
    </div>
</div>
<!-- partner section  startend-->


<!-- footer part start-->
<footer class="footer-area">
                
<div class="newsletter-wrap">
<div class="container">
    <div class="row">
        <div class="col-sm-3 lr"></div>
        <div class="col-sm-6 lr">
            <div class="titles center" data-aos="fade-up">Subscribe to our newsletter</div>
            <p class="newslt-caption">Write your email on the form below and click subscribe to get the update through our newsletters.</p>
            <div class="input-group">
                <input type="email" class="form-control myemail" value="" id="myemail" placeholder="Enter your email">
                <span class="input-group-btn">
                    <button class="btn btn-theme subscribe" id="subscribe" type="submit">Subscribe</button>
                </span>
            </div>
            <p id="feedback" style="margin:10px 0; padding:10px 10px; text-align:center; color:#fff; border:1px solid #eee; display:none;"></p>
        </div>
        <div class="col-sm-3 lr"></div>
        
  </div>
    </div>
</div>
</div>

<section class="section-6" id="contact" style="display:none;"> 
<div class="container" data-aos="fade-up" data-aos-duration="1300">
<div class="titles-w center">GET IN TOUCH</div>
<div class="conts center v-space">      
  <div class="foot">
    <h3>Receive free updates about all upcoming events and programs, tips, mentor advice and new trends in the tech community direct to your inbox.</h3>
  </div>       
    <div class="newsletter">
      <form>
          <div class="row">
              <div class="col-xs-12 lr">
                  <div class="input-group">
                      <input type="text" class="form-control" placeholder="Email Address">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-success">SIGN UP</button>
                      </span>
                  </div>
              </div>
          </div>
      </form>
  </div>
  <div class="foot">
    <h5>Smart Codes HQ | House No. 346 Mikocheni A, Senga,<br> Dar es Salaam, Tanzania</h5>
  </div>

  <div class="social">
    <a href="https://www.facebook.com/SmartLab255/" target="_blank" class="media"><i class="fa fa-facebook-f"></i></a>
    <a href="https://twitter.com/SmartLab255" target="_blank" class="media"><i class="fa fa-twitter"></i></a>
    <a href="https://www.instagram.com/smartlab255/" target="_blank" class="media"><i class="fa fa-instagram"></i></a>
    <a href="https://www.linkedin.com/company/smart-labtz/" target="_blank" class="media"><i class="fa fa-linkedin"></i></a>
    <a href="https://www.youtube.com/channel/UCdLWdmmPcWvNxWrVV_c54Ig" target="_blank" class="media"><i class="fa fa-youtube"></i></a>
  </div>
</div>
</div>
</section>

<section class="section-6" id="contacts" style="color:#fff;"> 
<div class="container">
<div class="titles-w center">Get in touch</div>
<div class="row">
     <div class="col-sm-3 lr">
        <div class="foot-2">
            <h4>Resources:</h4>
            <p>
                <p><a href="https://www.smartlab.co.tz/hire_space" style="color:#fff">Hire Space</a></p>
                <p><a href="https://www.smartlab.co.tz/startup" style="color:#fff">Startup</a></p>
                <p><a href="https://www.smartlab.co.tz/corporate" style="color:#fff">Corporate</a></p>
            </p>
        </div>
    </div>
    <div class="col-sm-3 lr">
        <div class="foot-2">
            <h4>Where we are:</h4>
            <p>Smart Codes HQ | House No. 346<br> Mikocheni A, Senga,<br> Dar es Salaam, Tanzania</p>
        </div>
    </div>
    <div class="col-sm-3 lr">
        <div class="foot-2">
            <h4>Contact Us:</h4>
            <p>
                <p><i class="fa fa-envelope"></i>&nbsp;&nbsp; <a href="mailto:ideas@smartlab.co.tz">ideas@smartlab.co.tz</a></p>
                <p><i class="fa fa-phone"></i>&nbsp;&nbsp; +255 786 675 487</p>
                <p><i class="fa fa-phone"></i>&nbsp;&nbsp; +255 222 775 801</p>
            </p>
        </div>
    </div>
    <div class="col-sm-3 lr">
        <div class="foot-2">
            <h4>Follow Us On:</h4>
            <p>
               <div class="social">
                <a href="https://www.facebook.com/SmartLab255/" target="_blank" class="media"><i class="fa fa-facebook-f"></i></a>
                <a href="https://twitter.com/SmartLab255" target="_blank" class="media"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/smartlab255/" target="_blank" class="media"><i class="fa fa-instagram"></i></a>
                <a href="https://www.linkedin.com/company/smart-labtz/" target="_blank" class="media"><i class="fa fa-linkedin"></i></a>
                <a href="https://www.youtube.com/channel/UCdLWdmmPcWvNxWrVV_c54Ig" target="_blank" class="media"><i class="fa fa-youtube"></i></a>
              </div>
            </p>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="foot-3">
            <small>Copyright © 2020 Smart Lab</small>
        </div>
    </div>
</div>
</div>
</section>
</footer>
<!-- footer part end-->


<!-- custom js -->
<script src="js/custom.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://www.smartlab.co.tz/js/parallax.min.js"></script>
<script src="https://www.smartlab.co.tz/animations/aos.js"></script>
<script src="https://www.smartlab.co.tz/slick/slick.min.js"></script>
<script src="https://www.smartlab.co.tz/js/custom.js"></script>

<script type="text/javascript">

$(document).ready(function(){
  $('#articles').hide();
      $('#activities').hide();
      $('#publications').show();
     

      $('#btnpublication').on('click', function(){
        $('#articles').hide();
          $('#activities').hide();
          $('#publications').fadeIn();
          console.log('hide acti, artic');
      });

      $('#btnarticle').click(function(e){
        e.preventDefault();
          $('#articles').fadeIn();
          $('#publications').hide();
          $('#activities').hide();
      });

      $('#btnactivity').click(function(e){
        e.preventDefault();
          $('#activities').fadeIn();
          $('#publications').hide();
          $('#articles').hide();

      });
});

  
$("#DateCountdown").TimeCircles();
            $("#CountDownTimer").TimeCircles({ time: { Days: { show: false }, Hours: { show: false } }});
            $("#PageOpenTimer").TimeCircles();
            
            var updateTime = function(){
                var date = $("#date").val();
                var time = $("#time").val();
                var datetime = date + ' ' + time + ':00';
                $("#DateCountdown").data('date', datetime).TimeCircles().start();
            }
            $("#date").change(updateTime).keyup(updateTime);
            $("#time").change(updateTime).keyup(updateTime);
            
            // Start and stop are methods applied on the public TimeCircles instance
            $(".startTimer").click(function() {
                $("#CountDownTimer").TimeCircles().start();
            });
            $(".stopTimer").click(function() {
                $("#CountDownTimer").TimeCircles().stop();
            });

            // Fade in and fade out are examples of how chaining can be done with TimeCircles
            $(".fadeIn").click(function() {
                $("#PageOpenTimer").fadeIn();
            });
            $(".fadeOut").click(function() {
                $("#PageOpenTimer").fadeOut();
            });





  </script>

<script>
    AOS.init({
      easing: 'ease-in-out-sine'
    });
 </script>
 <script type="text/javascript">
   $(document).ready(function(){
     $('.menu-btn').click(function(){
       $('.menu-list').stop().slideToggle().removeClass('hides');
     });
     $('.menu-list ul li a').click(function(){
       $('.menu-list').stop().addClass('hides');
     });
   });
  </script>

<script>
    AOS.init({
      easing: 'ease-in-out-sine'
    });
 </script>
 <script type="text/javascript">
   $(document).ready(function(){
     $('.menu-btn').click(function(){
       $('.menu-list').stop().slideToggle().removeClass('hides');
     });
     $('.menu-list ul li a').click(function(){
       $('.menu-list').stop().addClass('hides');
     });
   });
 </script>
 
 <script>
 $(function(){
   $('.more').click(function(){
       $('.texted').stop().removeClass('text-count');
       $('.more').stop().addClass('hid');
   });
 });
 </script>
 
 <script>
 //smooth scrolling---------------
   jQuery(document).ready(function($){
       $('a[href*=\\#]:not([href=\\#])').click(function() {
       if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname){
         var target = $(this.hash);
         var top = 130;
         target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
         if (target.length) {
           $('html,body').animate({
             scrollTop: target.offset().top
           }, 1000);
           return false;
         }
       }
       });
     });
 
 </script>
 <script>
     jQuery(function() {
       jQuery('a').each(function() {
         if (jQuery(this).attr('href')  ===  window.location.href) {
           jQuery(this).addClass('active');
         }
       });
     }); 
 </script>
 
 <script type="text/javascript">
    $(document).ready(function(){
     var top = $('.top-header');
     var sect = $('.section');
 
     $(document).scroll(function () {
         if ($(this).scrollTop() > 20) {
             top.addClass("green-top");
             sect.addClass("unsection");
         } else {
             top.removeClass("green-top");
             sect.removeClass("unsection");
         }
     });
    });
 </script>
 
 <script type="text/javascript">
    $(document).ready(function(){
 
     var top = $('.apply-2');
 
     $(window).scroll(function () {
         if ($(this).scrollTop() > 120) {
             top.addClass("apply-3");
         } else {
             top.removeClass("apply-3");
         }
     });
    });
 </script>
 <script type="text/javascript">
     $(document).ready(function(){
         $('#subscribe').click(function(){
             var email = $('#myemail').val();
             //alert(email);
             $.ajax({
                 type : 'POST',
                 url : 'https://www.smartlab.co.tz/subscribe',
                 data: {'email':email, '_token':'tFwyaMAgN1GlsFTIXXKeKgnw61AAp16flDub5UAp'},
                 success:function(data){
                     console.log(data);
                     $('#feedback').html(data).fadeIn();
                     $('#myemail').val('');
                 } 
             })
         });
     });
      //s;idersection start
        console.clear();
        $(".slider").slick({
            // autoplay: true,
            dots: true,
          slidesToShow: 1,
          slidesToScroll: 2
        });

     //activities section start

     function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    }

    document.getElementById("defaultOpen").click();

   //activities section end

   
 </script>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script><script  src="./script.js"></script>

</body>
</html>
